import { Post } from "leonardoassis-sdk";
import styled from "styled-components";
import Image from "next/image";
import { transparentize } from "polished";
import formatPostDate from "../core/utils/formatPostDate";

interface PostProps {
  thumbnail: string;
  createdAt: string;
  editor: Post.Detailed["editor"];
  title: string;
}

export default function PostHeader (props: PostProps) {
  
  return <Wrapper>
    
    <Thumbnail>
      <Image
        src={props.thumbnail}
        width={848}
        height={256}
        objectFit={"cover"}
        alt={props.title}
     />
    </Thumbnail>
    
    <Editor>
      <Image
        src={props.editor.avatarUrls.small}
        width={64}
        height={64}
        objectFit={"cover"}
        alt={props.editor.name}
      />
    </Editor>

    <PublishDate>{formatPostDate(props.createdAt)}</PublishDate>
    <Title>{props.title}</Title>
  </Wrapper>

  
}

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 16px;
  text-align: center;
`

const Editor = styled.div`
  width: 64px;
  height: 64px;
  border-radius: 32px;
  margin-top: -48px;
  position: relative;
  box-shadow: 0 0 0 4px ${(p) => p.theme.pageBackground};

  img {
    border-radius: 32px;
  }
  
`


const Thumbnail = styled.div`
  border-top-left-radius: ${(p) => p.theme.borderRadius};
  border-top-right-radius: ${(p) => p.theme.borderRadius};
  overflow: hidden;
  object-fit: cover;

  img {
    height: 100%;
    object-fit: cover;
  }
`

const PublishDate = styled.p`
  color: ${(p) => transparentize(0.5, p.theme.pageForeground)};
  font-size: 12px;
`;
const Title = styled.h1`
  font-size: 36px;
  font-weight: 600;
`;