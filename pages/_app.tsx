import { AppProps as NextAppProps } from 'next/app'
import { ThemeProvider } from 'styled-components'
import Content from '../components/Content'
import Footer from '../components/Footer'
import Header from '../components/Header'
import '../styles/globals.css'
import GlobalStyles from '../styles/globalStyles'
import { light } from '../styles/theme'
import Error from 'next/error'
import error from 'next/error'

interface CustomProps extends NextPageProps {}

type AppProps<P = any> = {
  pageProps: P
} & Omit<NextAppProps<P>, "pageProps">

function MyApp({ Component, pageProps }: AppProps<CustomProps>) {
  if (pageProps.error) {
    return <Error 
      title={pageProps.error.message}
      statusCode={pageProps.error.statusCode}/>
  }

  return <ThemeProvider theme={light}>
    <Header />
    <Content>
      <Component {...pageProps} />
    </Content>
    <Footer />
    <GlobalStyles />
  </ThemeProvider>
}

export default MyApp
