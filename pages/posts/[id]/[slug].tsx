import { Post, PostService } from "leonardoassis-sdk"
import { GetServerSideProps } from "next"
import { ParsedUrlQuery } from 'querystring'
import { ResourceNotFoundError } from "leonardoassis-sdk/dist/errors"
import Head from "next/head"
import PostHeader from '../../../components/PostHeader';
import Markdown from "../../../components/Markdown"

export default function PostPage (props: PostProps) {
  return <>
    <Head>
      <meta property="og:title" content={props.post.title}/>
      <meta property="og:site_name" content="AlgaNews"/>
      <meta property="og:url" content="alganews.com.br"/>
      <meta property="og:description" content={props.post.body.slice(0, 54)}/>
      <meta property="og:type" content="article"/>
      <meta property="og:image" content={props.post.imageUrls.medium}/>

      <title>{props.post.title} - AlgaNews</title>

      <link rel="canonical" href={`http://${props.host}/${props.post?.id}/${props.post?.slug}`} />
    </Head>
    <PostHeader 
      thumbnail={props.post.imageUrls.large}
      createdAt={props.post.createdAt}
      editor={props.post.editor}
      title={props.post.title}/>
    <Markdown>
      {props.post.body}
    </Markdown>
  </>
}

interface PostProps extends NextPageProps{
  post?: Post.Detailed;
  host?: string
}

interface Params extends ParsedUrlQuery {
  id: string;
  slug: string;
}

export const getServerSideProps: GetServerSideProps<PostProps, Params> =
  async ({params, res, req}) => {
    try{
      if (!params)
        return { notFound: true };
      
      const {id, slug} = params;
      const postId = Number(id);

      if (isNaN(postId))
        return { notFound: true };

      const post = await PostService.getExistingPost(postId);
      if (slug !== post.slug) {
        res.statusCode = 301;
        res.setHeader('Location', `/posts/${post.id}/${post.slug}`)
        return { props: {} }
      }

      return {
        props: {
          post,
          host: req.headers.host
        }
      }
    } catch (error) {
      if (error instanceof ResourceNotFoundError) {
        return { notFound: true};
      }
      return {
        props: {
          error: {
            message: error.message,
            statusCode: error.data?.status || 500
          },
        },
      };
    }
  };